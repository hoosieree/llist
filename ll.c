#include<assert.h>
#include<stdlib.h>
#include<stdio.h>
#define p printf
#define nl p("\n")
#define mem(T) malloc(sizeof(T))

typedef long long I;
typedef struct n0{struct n0* next; I data;} Node;

void pl(Node* h){for(Node* i=h;i;i=i->next){p("%lli ",i->data);}}
I len(Node* h){I i=0; for(;h;h=h->next){++i;} return i;}
I cnt(Node* h, I q){I i=0; for(;h;h=h->next){i+=h->data==q;} return i;}
Node* psh(Node** h, I d){Node* n=mem(Node); n->data=d; n->next=*h; return *h = n;}
Node* app(Node** h, I d){if(!*h){return psh(h,d);} Node** t=h; while(*t){t=&((*t)->next);} return psh(t,d);}
Node* nth(Node** h, I i){assert(len(*h)>=i && i>=0); Node** t=h; while(i-->0){t=&((*t)->next);} return *t;}
Node* del(Node** h){Node* t=*h; while(t->n){Node* n=t->n; free(t); t = n;} return *h = NULL;}

Node* iota(I n){Node* h=NULL; if(n>0){for(I i=n;i;i--){psh(&h,i-1);}} else{for(I i=-n;i>n-1;i--){psh(&h,i);}} return h;}
int main(){
  Node* h = iota(-9);
  p("created list: "); pl(h); nl;

  del(&h);
  p("deleted list: "); pl(h); nl;
  /* p("%lli ", nth(&h, 1)->data); nl; */
  return 0;
}
